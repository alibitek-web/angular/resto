import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from "@angular/forms";
import { RestoService } from '../resto.service';
import { Restaurant } from '../models/restaurant';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-update-restaurant',
  templateUrl: './update-restaurant.component.html',
  styleUrls: ['./update-restaurant.component.css']
})
export class UpdateRestaurantComponent implements OnInit {
  successMessage = '';
  failureMessage = '';
  defaultSuccessMessage = 'Restaurant updated successfully!';
  defaultFailureMessage = 'Error occurred while trying to update the restaurant!';
  invalidRestaurantMessage = "Invalid data! Restaurant name or address cannot be empty!"

  updateRestaurantFormGroup = new FormGroup({
    name: new FormControl(''),
    address: new FormControl(''),
    email: new FormControl(''),
  })

  constructor(private restoService: RestoService, private router: ActivatedRoute) { }

  ngOnInit(): void {
    this.restoService.getRestaurant(this.router.snapshot.params.id).subscribe((restaurant: Restaurant) => {
      this.updateRestaurantFormGroup = new FormGroup({
        name: new FormControl(restaurant.name),
        address: new FormControl(restaurant.address),
        email: new FormControl(restaurant.email),
      });
    });
  }

  updateRestaurant(): void {
    const restaurant = this.updateRestaurantFormGroup.value as Restaurant;
    if (restaurant.name === '' || restaurant.address === '') {
      this.successMessage = '';
      this.failureMessage = this.invalidRestaurantMessage;
      return;
    }

    restaurant.id = this.router.snapshot.params.id;

    this.restoService.updateRestaurant(restaurant).subscribe(
      (result: Restaurant) => {      
        if (!result) {
          this.successMessage = '';
          this.failureMessage = this.defaultFailureMessage;
        } else {
          this.failureMessage = '';
          this.successMessage = this.defaultSuccessMessage;
        }
        this.updateRestaurantFormGroup.reset({});
      },
      (error: any) => {
          this.failureMessage = `${this.defaultFailureMessage} [ Reason: ${error.message} ]`;
      });    
  }

}
