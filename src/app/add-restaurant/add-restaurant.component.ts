import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from "@angular/forms";
import { RestoService } from '../resto.service';
import { Restaurant } from '../models/restaurant';

@Component({
  selector: 'app-add-restaurant',
  templateUrl: './add-restaurant.component.html',
  styleUrls: ['./add-restaurant.component.css']
})
export class AddRestaurantComponent implements OnInit {
  addRestaurantFormGroup = new FormGroup({
    name: new FormControl(''),
    address: new FormControl(''),
    email: new FormControl(''),
  })

  successMessage = '';
  failureMessage = '';

  defaultSuccessMessage = 'Restaurant added successfully!';
  defaultFailureMessage = 'Error occurred while trying to add the restaurant!';
  invalidRestaurantMessage = "Invalid data! Restaurant name or address cannot be empty!"

  constructor(private restoService: RestoService) { }

  ngOnInit(): void {
  }

  addRestaurant(): void {
    const restaurant = this.addRestaurantFormGroup.value as Restaurant;
    if (restaurant.name === '' || restaurant.address === '') {
      this.successMessage = '';
      this.failureMessage = this.invalidRestaurantMessage;
      return;
    }

    this.restoService.addRestaurant(restaurant).subscribe(
      (result: Restaurant) => {      
        if (!result) {
          this.successMessage = '';
          this.failureMessage = this.defaultFailureMessage;
        } else {
          this.failureMessage = '';
          this.successMessage = this.defaultSuccessMessage;
        }
        this.addRestaurantFormGroup.reset({});
      },
      (error: any) => {
          this.failureMessage = `${this.defaultFailureMessage} [ Reason: ${error.message} ]`;
      });    
  }
}