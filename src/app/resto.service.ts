import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Restaurant } from './models/restaurant';
import { HttpHeaders } from '@angular/common/http';
import { User } from './models/User';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RestoService {  
  rootUrl = "http://localhost:3000";
  restaurantsUrl = `${this.rootUrl}/restaurants`;
  usersUrl = `${this.rootUrl}/users`;

  constructor(private httpClient: HttpClient) { }

  getRestaurants(): Observable<Restaurant[]> {
    return this.httpClient.get<Restaurant[]>(this.restaurantsUrl);
  }

  addRestaurant(restaurant: Restaurant): Observable<Restaurant> {
    return this.httpClient.post<Restaurant>(this.restaurantsUrl, restaurant, httpOptions);
  }

  deleteRestaurant(restaurant: Restaurant): Observable<any> {
    const url = `${this.restaurantsUrl}/${restaurant.id}`;
    return this.httpClient.delete(url);
  }

  updateRestaurant(restaurant: Restaurant): Observable<Restaurant> {
    const url = `${this.restaurantsUrl}/${restaurant.id}`;
    return this.httpClient.put<Restaurant>(url, restaurant, httpOptions);
  }

  getRestaurant(id: number): Observable<Restaurant> {
    const url = `${this.restaurantsUrl}/${id}`;
    return this.httpClient.get<Restaurant>(url);
  }

  registerUser(user: User): Observable<User> {
    return this.httpClient.post<User>(this.usersUrl, user, httpOptions);
  }
}
