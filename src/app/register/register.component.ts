import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from "@angular/forms";
import { RestoService } from '../resto.service';
import { User } from '../models/User';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerUserFormGroup = new FormGroup({
    name: new FormControl(''),    
    email: new FormControl(''),
    password: new FormControl(''),
  })

  successMessage = '';
  failureMessage = '';

  defaultSuccessMessage = 'User registered successfully!';
  defaultFailureMessage = 'Error occurred while trying to register the user!';
  invalidUserMessage = "Invalid data! User name, email or password cannot be empty!";

  constructor(private restoService: RestoService) { }

  ngOnInit(): void {
  }

  registerUser(): void {
    const user = this.registerUserFormGroup.value as User;
    if (user.name === '' || user.email === '' || user.password === '') {
      this.successMessage = '';
      this.failureMessage = this.invalidUserMessage;
      return;
    }

    this.restoService.registerUser(user).subscribe(
      (result: User) => {      
        if (!result) {
          this.successMessage = '';
          this.failureMessage = this.defaultFailureMessage;
        } else {
          this.failureMessage = '';
          this.successMessage = this.defaultSuccessMessage;
        }
        this.registerUserFormGroup.reset({});
      },
      (error: any) => {
          this.failureMessage = `${this.defaultFailureMessage} [ Reason: ${error.message} ]`;
      });    
  }

}
