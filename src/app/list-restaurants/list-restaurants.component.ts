import { Component, OnInit } from '@angular/core';
import { RestoService } from '../resto.service';
import { Restaurant } from '../models/restaurant';
import { faTrash, faEdit } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-list-restaurants',
  templateUrl: './list-restaurants.component.html',
  styleUrls: ['./list-restaurants.component.css']
})
export class ListRestaurantsComponent implements OnInit {
  restaurants: Restaurant[];
  faTrash = faTrash;
  faEdit = faEdit;
  
  constructor(private restoService: RestoService) { }

  ngOnInit(): void {
    this.restoService.getRestaurants().subscribe(restaurants => this.restaurants = restaurants);
  }

  deleteRestaurant(restaurant: Restaurant): void {
    if(confirm(`Do you really want to delete restaurant: ${restaurant.name}?`))
    {
      this.restoService.deleteRestaurant(restaurant).subscribe(() => {
        this.restaurants.splice(this.restaurants.indexOf(restaurant), 1);
      });
    }
  }
}
